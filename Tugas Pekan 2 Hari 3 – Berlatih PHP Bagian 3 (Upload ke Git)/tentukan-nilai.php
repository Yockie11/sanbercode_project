<?php
function tentukan_nilai($number)
{
    $output = "";
    if ($number >= 85 and $number <= 100) {
        echo "Sangat Baik";
    } elseif ($number >= 70) {
        echo "Baik";
    } elseif ($number >= 60) {
        echo "Cukup";
    } elseif ($number < 60) {
        echo "Kurang";
    }
    return $output . "<br>";
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
